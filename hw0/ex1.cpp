#include <iostream>

int recur_gcd(int a, int b){
  if (b == 0){
    return a;
  }
  else{
    return recur_gcd(b, a % b);
  }
}

int loop_gcd(int a, int b){
  int t;
  while (b != 0){
    t = b;
    b = a % b;
    a = t;
  }
  return a;
}

int main(){
  int a,b;
  a = recur_gcd(18,12);
  b = loop_gcd(18,12);

  printf("%i \n %i\n", a,b);
  return 0;
}
  
