#include <iostream>

int recur_Fibo(int m){
  if (m == 0){
    return 0;
  } else if (m == 1){
    return 1;
  } else{
    return recur_Fibo(m-1) + recur_Fibo(m-2);
  }
}

int loop_Fibo(int m){
  int k = 0;
  int a = 0; // F_{n-2}
  int b = 1; // F_{n-1}
  if (m == 0){
    k = 0;
  } else if (m == 1){
    k = 1;
  } else{
    for (int i = 1; i < m; i++){
    a = b;
    b = a;
    k = a + b;
    }
  }
  return k;
}

int main(){
  int a, b;
  a = recur_Fibo(82);
  b = loop_Fibo(82);
  printf("%i \n %i \n", a, b);
  return 0;
}
