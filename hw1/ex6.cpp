#include <stdio.h>
#include <math.h> 
#include <iostream>

int simpson_integrate(double a, double b){
  int N = 200;
  double x = a;
  double delta = (b-a)/N;
  double integ = 0;
  for (int i = 0; i < N; i++){
    integ += delta/6*(sin(x) + 4*sin(x+delta/2) + sin(x+delta));
    x += delta;

  }

  return integ;
}

int main(){

  double integ = simpson_integrate(0.0, M_PI);
  printf("%f\n" ,integ);

  return integ; 
}



