#include <iostream>

float machine_precision(){

  float epsilon = 1.0;
  while (1.0 + 0.5*epsilon != 1.0) {
    epsilon = 0.5 * epsilon;
  }

  return epsilon;
}

int main(){
  
  float a;
  a = machine_precision();

  std::cout << a << std::endl;
  return a;
}

